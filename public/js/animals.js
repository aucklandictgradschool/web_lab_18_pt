"use strict";

function getLongDescription(id) {
    $.get("/animals/" + id, function (data, status) {
        $("#animal-description-" + id).html(data);
    });
}